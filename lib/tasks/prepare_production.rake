
namespace :db do
  task :prepare do
    set :environment, :production

    run './config/create_productiondb.sh'
    Rake::Task['db:setup']
  end
end

#!/bin/bash

echo 'Digite a senha do usuário root do banco de dados'
read ROOT_PASS

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password ${ROOT_PASS}'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password ${ROOT_PASS}'
sudo apt-get -y install mysql-server

MYSQL=`which mysql`

Q1="CREATE DATABASE IF NOT EXISTS globocom_production;"
Q2="GRANT USAGE ON *.* TO globocom@localhost IDENTIFIED BY 'selecaoglobocom';"
Q3="GRANT ALL PRIVILEGES ON globocom_production.* TO globocom@localhost;"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

$MYSQL -u root -p -e "$SQL"
echo 'passou aqui'
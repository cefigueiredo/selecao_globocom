20-09-2014
Duvida sobre que arquitetura utilizar: Rails com dependências que o projeto sequer usará, ou Sinatra configurando Sprockets e ActiveRecord na mão. Primeiramente, decidi por Rails por já ter um ambiente preconfigurado e poder focar somente no sistema de votação. Caso no futuro isso venha a ser um problema na performance, migrarei para Sinatra, que é mais leve.

21-09-2014
Criei um modelo Contestant para registrar os participantes do BBB que estão disputando o paredão e conseguir associar ao voto. Registrar somente o nome do participante foi considerado, mas dificulta ao listar todos os possíveis participantes que podem ser associados a algum voto.

Criei um modelo de Contest, para representar o Paredão que terá os participantes para serem votados.

Indice: Decidi não usar nenhum indice até o momento, pois é esperado que a tabela de votos tenha uma quantidade de inserções muito alta e frequente. E  caso mantivesse um indice, o banco o teria que reindexar a cada inserção, o que degradaria em muito a performance na criaçao de votos ao longo do tempo.

24-09-2014
Substituição do servidor de aplicacao padrao do Rails, o Webrick, pelo Puma. Por ser um servidor que alem de baixo consumo, tem poder de utilizar threads e workers por processador da maquina.

25-09-2014
Tentei adicionar o gráfico da pagina de agradecimento, mas sem sucesso.

Defini pool do banco como 15, pois com 6 Threads e 2 workers, a previsao é de rodar até 12 conexoes simultaneas, evitando que o Puma nao consiga abrir uma conexao.

28-09-2014
Adicionei scopes para descobrir qual o Paredão vigente.

Adição de Memcached para cachear na memoria do servidor buscas que seriam repetidas por muitas requisicoes com o mesmo resultado.

Montado o Script de Deploy:
  O script está preparado para ser executado em ambiente Linux Ubuntu ou compativel com sistemas de pacotes Debian
  Para efetuar deploy da app, é preciso rodar o comando

  ```bash
      ./prepara-ambiente.sh
  ```

  O script já vai instalar os requisitos, que são: Ngnix, Mysql Server e Memcached.
  Quando necessário o script vai exibir um prompt para inserir senhas.

Iniciando a aplicacao:
  para iniciar a aplicação é preciso rodar o comando:

  ```bash
      foreman start
  ```

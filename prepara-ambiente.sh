#!/bin/bash

sudo apt-get install -q -y nginx memcached lib-mysqlclient

echo 'Configurando banco de dados'
./config/create_productiondb.sh

echo 'Configurando Nginx'
PROJECT_PATH=`pwd`
sudo rm /etc/nginx/sites-enabled/default
sudo ln -s $PROJECT_PATH/config/nginx-globo.conf /etc/nginx/sites-enabled
cat > ./config/nginx-globo.conf <<_EOF_
upstream puma {
  server localhost:3000;
}
server {
  listen 80 default deferred;

  root $PROJECT_PATH/public;
  try_files \$uri/index.html \$uri @puma;

  location ~* .(jpg|jpeg|png|gif|ico|css|js)$ {
    expires 365d;
  }

  location @puma {
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header Host \$http_host;
    proxy_redirect off;
    proxy_pass http://puma;
  }

  error_page 500 502 503 504 /500.html;
  client_max_body_size 4G;
  keepalive_timeout 10;
}
_EOF_

echo 'Instalando dependencias'
bundle install

SECRET_KEY_BASE=`rake secret`

export SECRET_KEY_BASE=$SECRET_KEY_BASE
echo 'export SECRET_KEY_BASE=${SECRET_KEY_BASE}' >> ~/.bash_profile
rake db:setup RACK_ENV=production
rake assets:precompile RACK_ENV=production
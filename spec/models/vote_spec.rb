require 'rails_helper'
require 'benchmark'

describe Vote, :type => :model do
  let(:vote) { Vote.new }

  it 'has a session associated' do
    vote.session = 'session1'
    expect(vote.session).to eq 'session1'
  end

  it 'has a contestant associated' do
    contestant = Contestant.new
    vote.contestant = contestant

    expect(vote.contestant).to eql(contestant)
  end

  it 'belongs to a contest' do
    contest = Contest.new
    vote.contest = contest

    expect(vote.contest).to eq contest
  end

  describe 'valid?' do
    let(:session) { 'session1' }
    let(:contest) { Contest.new }
    let(:valid_contestant) { contest.contestants.build name: 'participant1' }

    context('when bypassing human being check') do
      before { vote.bypass_humanizer = true }

      it 'all attributes is filled' do
        vote.session = session
        vote.contest = contest
        vote.contestant = valid_contestant

        expect(vote.valid?).to be_truthy
      end

      it 'contestant belongs to contest' do
        vote.session = session
        vote.contest = contest
        vote.contestant = Contestant.new name: 'participant2'

        expect(vote.valid?).to be_falsy
      end
    end

    context('when belongs to a human') do
      before do
        vote.session = session
        vote.contest = contest
        vote.contestant = valid_contestant
      end

      it 'asks for human being answer' do
        expect(vote.humanizer_question_id).to_not be_falsy
        expect(vote.valid?).to be_falsy

        vote.humanizer_question_id = 0
        vote.humanizer_answer = '4'

        expect(vote.valid?).to be_truthy
      end
    end
  end

  describe "benchmark test" do
    let(:contest) { Contest.create }
    let(:contestant1) { contest.contestants.create id: 1}

    it "creates 100 votes in 1 second" do
      expect(Benchmark.realtime{
        100.times do |t|
          Vote.create(
            contestant: contestant1,
            contest: contest,
            bypass_humanizer: true)
        end
      }).to be <= 1
    end
  end
end

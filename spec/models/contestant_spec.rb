require 'rails_helper'

describe Contestant, :type => :model do
  let(:contestant) { Contestant.new }

  it 'has a name' do
    contestant.name = 'participant1'

    expect(contestant.name).to eq 'participant1'
  end

  it 'has a gender' do
    contestant.gender = 'male'

    expect(contestant.gender).to eq 'male'
  end

  it 'belongs to contest' do
    contest = Contest.new
    contestant = contest.contestants.build

    expect(contestant.contest).to eq contest
  end
end

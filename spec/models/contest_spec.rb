require 'rails_helper'

RSpec.describe Contest, :type => :model do
  let(:contest) { Contest.new }

  it 'has a date' do
    contest_date = Date.new
    contest.date = contest_date

    expect(contest.date).to eq contest_date
  end

  it 'has votes associated' do
    contest.votes.build session: 'supporter1'
    contest.votes.build session: 'supporter2'

    expect(contest.votes.size).to eq 2
  end

  it 'has contestants associated' do
    contest.contestants.build name: 'participant1'
    contest.contestants.build name: 'participant2'

    expect(contest.contestants.size).to eq 2
  end

  it 'has a current flag' do
    contest.current = true

    expect(contest.current).to be_truthy
  end

  it 'only one can be current' do
    contest.current = true
    contest2 = Contest.create current: true

    expect(contest.valid?).to be_falsy
  end
end

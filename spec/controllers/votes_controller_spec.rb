require 'rails_helper'
require 'benchmark'

RSpec.describe VotesController, :type => :controller do
  let(:contest) { Contest.create id: 99, current: true }
  before do
    Rails.cache.clear
    2.times { |t| contest.contestants.create id: t + 1 }
  end

  controller do
    def resource_params
      params.require(:vote)
        .permit( :contestant_id,
                 :humanizer_question_id,
                 :humanizer_answer)
        .merge(session: 1, bypass_humanizer: true)
    end
  end

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST create" do
    it "returns http success" do
      post :create, vote: { contestant_id: 1 }
      expect(response).to have_http_status(:success)
      expect(Vote.count).to eq(1)
    end

    context "benchmark 100 votes" do
      let(:vote_times) { 100 }

      def vote_many_times
        vote_times.times do |t|
          post :create, vote: { contestant_id: 1 }
        end
      end

      it "can create 100 votes" do
        vote_many_times
        expect(Vote.count).to eq(100)
      end

      it "can hold it on 1second or less" do
        expect(Benchmark.realtime{vote_many_times}).to be <= 1
      end
    end
  end
end

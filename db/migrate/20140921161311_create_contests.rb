class CreateContests < ActiveRecord::Migration
  def change
    create_table :contests do |t|
      t.date :date

      t.timestamps
    end

    change_table :votes do |t|
      t.references :contest
    end
  end
end

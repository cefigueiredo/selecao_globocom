class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.string :session

      t.timestamps
    end
  end
end

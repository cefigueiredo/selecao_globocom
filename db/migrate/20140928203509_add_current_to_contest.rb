class AddCurrentToContest < ActiveRecord::Migration
  def change
    add_column :contests, :current, :boolean, unique: true
  end
end

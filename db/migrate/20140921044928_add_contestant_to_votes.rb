class AddContestantToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :contestant_id, :integer
  end
end

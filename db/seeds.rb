# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
contest = Contest.create(date: Date.new(2014, 9, 30), current: true)
contest.contestants.create([{name: 'Participante 1', gender: 'female'}, {name: 'Participante 2', gender: 'male'}])
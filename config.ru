# This file is used by Rack-based servers to start the application.

class Api
  def initialize app
    @app = app
  end
  def call(env)
    req = Rack::Request.new env
    command = req.POST["vote"].merge({session: 1, bypass_humanizer: true})
    puts "passou config.ru"
    result = contest.votes.create(command)
   [200, {"Content-Type" => "text/json"}, [result]]
  end

  def contest
    @contest ||= Rails.cache.fetch([:current, :contest], expires_in: 1.hour) do
      Contest.current
    end
  end

end
map "/vote_test" do
  use  Api
end


require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application

class Vote < ActiveRecord::Base
  include Humanizer
  require_human_on :create, unless: :bypass_humanizer

  attr_accessor :bypass_humanizer

  belongs_to :contestant, inverse_of: :votes
  belongs_to :contest, inverse_of: :votes

  validates :session, :contest, :contestant, presence: true
  validates :contestant, inclusion: { in: -> (vote) { vote.contest.contestants } }
end

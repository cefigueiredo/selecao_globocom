class Contestant < ActiveRecord::Base
  has_many :votes, inverse_of: :contestant
  belongs_to :contest, inverse_of: :contestants
end

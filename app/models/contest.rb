class Contest < ActiveRecord::Base
  has_many :votes, inverse_of: :contest
  has_many :contestants, inverse_of: :contest

  scope :current, -> { find_by(current: true) }

  validates :current, uniqueness: true

  before_save :ensure_current_consistency

  accepts_nested_attributes_for :votes

  def cached_contestants
    Rails.cache.fetch([:contest, id, :contestants], expires_in: 10.minutes) do
      contestants
    end
  end
  private
  def ensure_current_consistency
    if !self.current
      self.current = nil
    end
  end
end

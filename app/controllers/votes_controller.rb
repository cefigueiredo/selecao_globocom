class VotesController < ApplicationController
  def index
    @contestants = contest.cached_contestants
    @vote = Vote.new
  end

  def create
    puts "nao deveria ter passado"
    @vote = contest.votes.create(resource_params)
    unless @vote.valid?
      flash[:danger] = 'Seu voto não pôde ser computado.'
    end
    @partial_result = partial_result
    respond_with(@vote)
  end

  private
  def resource_params
    params.require(:vote)
      .permit( :contestant_id,
               :humanizer_question_id,
               :humanizer_answer)
      .merge(session: request.session_options[:id])
  end

  def contest
    @contest ||= Rails.cache.fetch([:current, :contest], expires_in: 1.hour) do
      Contest.current
    end
  end

  def partial_result
    contestants = contest.cached_contestants
    {
      contestant_votes: contestants.map do |contestant|
        contestant.attributes.merge(votes: contestant.votes.count)
      end,
      total_votes: contest.votes.count
    }
  end
end

# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

renderChart = ->
  ctx = $("#partial_result")[0].getContext("2d")
  data = $.parseJSON $("#partial_result").attr("data-partial-result")
  data.contestantPercentage = (index) ->
    prc = this.contestant_votes[index].votes / this.total_votes
    return Math.floor(prc * 100)

  chartData = [
    { value: data.contestantPercentage(0), color: "#F7464A", highlight: "#FF5A5E", label: data.contestant_votes[0].name},
    { value: data.contestantPercentage(1), color: "#46BFBD", highlight: "#5AD3D1", label: data.contestant_votes[1].name}
  ]

  chartConfig = {
    responsive: false,
    percentageInnerCutout: 70,
    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>%",
  }
  resultChart = new Chart(ctx).Doughnut(chartData, chartConfig)
  resultChart.generateLegend()

$('.vote.result').ready(renderChart)
$('.vote.result').on('page:load', renderChart)